
# Instalasi Aplikasi

Versi laravel yang digunakan adalah versi 6.10.

## *Clone Repository*

Masuk ke direktori kerja lalu *clone* repositori.

```
cd /public_html/
git clone {REPO_URL}
```

## *Instalasi Package* 

```
composer install
```


# Konfigurasi

## *Set Database MySQL*

Contoh pengaturan database MySQL.

* db_name = transisi
* username  = root
* password = root

## *Berkas .env*

Berkas konfigurasi disimpan pada `.env`. Salin berkas `.env.example` kemudian sunting informasi sesuai kebutuhan.

```
cp .env.example.env
```

## *Buat Kunci Enkripsi*

Buat kunci enkripsi.

```
php artisan key:generate
```

## *Contoh Berkas `.env`*

```
APP_NAME=Transisi
APP_ENV=local
APP_KEY=base64:AxQu7/9MUdZ0CJWw2ELLNy61OP788fxHVFHM3xXVXBM=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=transisi
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=cookie
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

```

## *Seed Akun admin*
untuk generate akun login admin

```
php artisan db:seed
```

Akun login admin

* username  = admin@transisi.id
* password = transisi