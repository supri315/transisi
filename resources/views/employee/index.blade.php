


@extends('layouts.app')

@section('content')

<div class="container-fluid ">
  <a href="{{ route('employee.create')}}" class="btn btn-success float-right mb-4">Tambah Employee</a>
  <h4>Daftar Employee</h4>
    <br>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   <table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Company</th>
      <th scope="col">Email</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    @foreach($employees as $key => $employee)
    <tr>
      <th>{{ $employees->firstItem() + $key }}</th>
      <td>{{ $employee->nama }}</td>
      <td>{{ $employee->company->nama }}</td>
      <td>{{ $employee->email }}</td>
      <td>  
        <a href="{{ route('employee.edit',$employee->id) }}" class="btn btn-dark btn-sm"><i class="fa fa-edit"></i></a> 
        <a href="{{ route('employee.show',$employee->id) }}" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i></a> 
        <a href="javascript:;" data-toggle="modal" onclick="deleteData({{$employee->id}})" data-target="#DeleteModal" class="btn btn-sm btn-dark"><i class="fa fa-trash"></i></a>
      </td>
    </tr>
    @endforeach
  
  </tbody>
</table>
</div>

@include('company.modal')



@endsection



@push('scripts')

<script type="text/javascript">
    function deleteData(id)
    {
         var id = id;
         var url = '{{ route("employee.delete", ":id") }}';
         url = url.replace(':id', id);
         $("#deleteForm").attr('action', url);
     }

     function formSubmit()
     {
         $("#deleteForm").submit();
     }
 </script>

  @endpush