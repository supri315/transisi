@extends('layouts.app')
@section('content')
<style>
    .btn-success{
    position:relative;
    left:50%
    }
</style>

<main class="container">
    <a href="{{ route('employee.list') }}" class="btn btn-primary btn-sm" style="float: right;">Kembali</a>
    <h4>Data Employee</h4>
    <hr />

    <form>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Nama</label>
                    <input type="text" class="form-control" name="nama" placeholder="Nama" value="{{ $employee->nama}}">
                </div>

                <div class="form-group">
                    <label >Email</label>
                    <input type="text" class="form-control"  name="email" placeholder="Email" value="{{ $employee->email}}">
                   
                </div>                          
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label >Company</label>
                    <input type="text" class="form-control"  name="Company" placeholder="" value="{{  $employee->company->nama }}">

                </div>
            </div>
        </div>
        <hr>

    </form>
</main>

@endsection
