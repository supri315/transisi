@extends('layouts.app')
@section('content')
<style>
    .btn-success{
    position:relative;
    left:50%
    }
</style>

<main class="container">
    <a href="{{ route('employee.list') }}" class="btn btn-primary btn-sm" style="float: right;">Kembali</a>

    <h4>Tambah Employee</h4>
    <hr />

    <form action = "{{ route('employee.update',$employee->id) }}" role="form" method="post" >
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Nama</label>
                    <input type="text" class="form-control" name="nama" placeholder="Nama" value="{{ $employee->nama}}">
                </div>

                <div class="form-group">
                    <label >Email</label>
                    <input type="text" class="form-control"  name="email" placeholder="Email" value="{{ $employee->email}}">
                   
                </div>                          
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label >Company</label>
                    <select class="form-control" name="company_id">
                        @foreach($company as $company)
                          <option value="{{ $company->id }}"
                            <?php if($company->id == $employee->company_id) { echo "selected";} ?> >
                                {{$company->nama}}
                        </option>
                        @endforeach
                    </select>
                    @if ($errors->has('company_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('company_id') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <button type="submit" class="btn btn-success">SIMPAN</button>
        </div>
    </form>
</main>

@endsection
