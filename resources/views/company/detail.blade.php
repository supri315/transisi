@extends('layouts.app')
@section('content')
<style>
    .btn-success{
    position:relative;
    left:50%
    }
</style>

<main class="container">
    <a href="{{ route('company.list') }}" class="btn btn-primary btn-sm" style="float: right;">Kembali</a>
    <h4>Data Company</h4>
    <hr />

    <form>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Nama</label>
                    <input type="text" class="form-control" name="nama" placeholder="Nama" value="{{ $company->nama}}">
                </div>

                <div class="form-group">
                    <label >Email</label>
                    <input type="text" class="form-control"  name="email" placeholder="Email" value="{{ $company->email }}">
                </div> 

                <div class="form-group">
                    <label >Website</label>
                    <input type="text" class="form-control"  name="website" placeholder="Website" value="{{ $company->website}}">
                </div>                         
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label >Logo</label><br>
                    <img src="{{ Storage::url('company/'.$company->logo ) }}" alt="{{ $company->logo }}" height="200" width="400">

                </div> 

            </div>
        </div>


        <hr>
    </form>
</main>

@endsection
