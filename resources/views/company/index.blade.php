@extends('layouts.app')

@section('content')

<div class="container-fluid ">
  <a href="{{ route('company.create')}}" class="btn btn-success float-right mb-4">Tambah Company</a>
  <h4>Daftar company</h4>
    <br>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   <table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Email</th>
      <th scope="col">Website</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    @foreach($companies as $key => $company)
    <tr>
      <th>{{ $companies->firstItem() + $key }}</th>
      <td>{{ $company->nama }}</td>
      <td>{{ $company->email }}</td>
      <td>{{ $company->website }}</td>
      <td>  
        <a href="{{ route('company.edit',$company->id) }}" class="btn btn-dark btn-sm"><i class="fa fa-edit"></i></a> 
        <a href="{{ route('company.show',$company->id) }}" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i></a> 
        <a href="javascript:;" data-toggle="modal" onclick="deleteData({{$company->id}})" data-target="#DeleteModal" class="btn btn-sm btn-dark"><i class="fa fa-trash"></i></a>
      </td>
    </tr>
    @endforeach
  
  </tbody>
</table>
</div>

@include('company.modal')



@endsection



@push('scripts')

<script type="text/javascript">
    function deleteData(id)
    {
         var id = id;
         var url = '{{ route("company.delete", ":id") }}';
         url = url.replace(':id', id);
         $("#deleteForm").attr('action', url);
     }

     function formSubmit()
     {
         $("#deleteForm").submit();
     }
 </script>

  @endpush