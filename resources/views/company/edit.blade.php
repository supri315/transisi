@extends('layouts.app')
@section('content')
<style>
    .btn-success{
    position:relative;
    left:50%
    }
</style>

<main class="container">
    <a href="{{ route('company.list') }}" class="btn btn-primary btn-sm" style="float: right;">Kembali</a>
    <h4>Edit Company</h4>
    <hr />

    <form action = "{{ route('company.update',$company->id) }}" role="form" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Nama</label>
                    <input type="text" class="form-control" name="nama" placeholder="Nama" value="{{ $company->nama}}">
                    @if ($errors->has('nama'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                    @endif 
                </div>

                <div class="form-group">
                    <label >Email</label>
                    <input type="text" class="form-control"  name="email" placeholder="Email" value="{{ $company->email }}">
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>                          
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label >Logo</label>
                    <input type="file" class="form-control"  name="logo" placeholder="Logo" >
                    @if ($errors->has('logo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('logo') }}</strong>
                    </span>
                    @endif
                </div> 

                <div class="form-group">
                    <label >Website</label>
                    <input type="text" class="form-control"  name="website" placeholder="Website" value="{{ $company->website}}">
                    @if ($errors->has('website'))
                    <span class="help-block">
                        <strong>{{ $errors->first('website') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <button type="submit" class="btn btn-success">SIMPAN</button>
        </div>
    </form>
</main>

@endsection
