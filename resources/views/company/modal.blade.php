<div class="modal fade" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteButton">
   <div class="modal-dialog"  role="document">
     <form action="" id="deleteForm" method="post">
         <div class="modal-content">
               <div class="modal-header">                            
                <h4 class="modal-title" id="deleteButton">Hapus Company</h4>
                </div>
             <div class="modal-body">
                 {{ csrf_field() }}
                 {{ method_field('DELETE') }}
                    <span>Apakah Anda yakin ingin menghapus ?</span>
             </div>
             <div class="modal-footer">
                 <center>
                     <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                     <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()">Yes, Delete</button>
                 </center>
             </div>
         </div>
     </form>
   </div>
  </div>