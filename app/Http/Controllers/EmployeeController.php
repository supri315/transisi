<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Company;
use App\Http\Requests\EmployeeStoreRequest;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $employees = Employee::with('company')->paginate(5);

        return view('employee.index', compact('employees'));
    }

    public function create()
    {
        $companies = Company::all();
        return view('employee.create',compact('companies'));
   
    }

    public function store(EmployeeStoreRequest $request)
    {
        Employee::create([
            'nama' => request('nama'),
            'email' => request('email'),
            'company_id' => request('company_id')        
        ]);

        return redirect()->route('employee.list')->with('success','Employee berhasil di tambah');;
    }

    public function show($id)
    {
        $employee = Employee::with('company')->find($id);

        return view('employee.detail', compact('employee'));
    }

    public function edit($id)
    {
        $employee = Employee::with('company')->find($id);

        $company = Company::all();

        return view('employee.edit', compact('employee','company'));
    }

    public function update(Request $request, $id)
    {
        
        $employee = Employee::find($id);
   
        $employee->update([
            'nama' => request('nama'),
            'email' => request('email'),
            'company_id' => request('company_id')        
        ]);

        return redirect()->route('employee.list')->with('success','Employee berhasil di ubah');;


    }

    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();

        return redirect()->route('employee.list')->with('success','Employee berhasil di hapus');;


    }
}
