<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Http\Requests\CompanyStoreRequest;
use App\Http\Requests\CompanyUpdateRequest;


class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $companies =  Company::paginate(5);

        return view('company.index', compact('companies'));
    }

    public function create()
    {
        return view('company.create');

    }


    public function store(CompanyStoreRequest $request)
    {

        $imageName = time().'.'.request()->logo->getClientOriginalExtension();

        $path = request()->logo->storeAs('public/company', $imageName);

        Company::create([
            'nama' => request('nama'),
            'email' => request('email'),
            'logo' => $imageName,
            'website' => request('website')        
        ]);


        return redirect()->route('company.list')->with('success','Company berhasil di tambah');;

    }

    public function show($id)
    {
        $company = Company::find($id);

        return view('company.detail', compact('company'));


    }

    public function edit($id)
    {
        $company = Company::find($id);

        return view('company.edit', compact('company'));
    }

    public function update(CompanyUpdateRequest $request, $id)
    {
        $company = Company::find($id);

        if ($request->hasFile('logo')) {

        $imageName = time().'.'.request()->logo->getClientOriginalExtension();

        $path = request()->logo->storeAs('company', $imageName);

        $company->update([
            'nama' => request('nama'),
            'email' => request('email'),
            'logo' => $imageName,
            'website' => request('website')  
        ]); 

        }else{
        $company->update([
            'nama' => request('nama'),
            'email' => request('email'),
            'website' => request('website')  
        ]); 

        }

        return redirect()->route('company.list')->with('success','Company berhasil di ubah');;


    }

    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();

        return redirect()->route('company.list')->with('success','Company berhasil di hapus');
    }
}
