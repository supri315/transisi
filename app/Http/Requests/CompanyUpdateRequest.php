<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nama'=>'required',
            'email'=>'required|email',
        ];
    }

    public function messages()
    {
        return [
            'nama.required' => 'Kolom nama harus di isi.',
            'email.required' => 'Kolom email harus di isi.',
            'email.email' => 'Kolom email harus mengandung karakter @.',
        ];
    }
}
