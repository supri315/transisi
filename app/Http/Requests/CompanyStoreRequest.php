<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nama'=>'required',
            'email'=>'required|email',
            'logo'=>'required|dimensions:min_width=100|dimensions:min_height=100|mimes:png|max:2000',
        ];
    }

    public function messages()
    {
        return [
            'nama.required' => 'Kolom nama harus di isi.',
            'email.required' => 'Kolom email harus di isi.',
            'email.email' => 'Kolom email harus mengandung karakter @.',
            'logo.required'=> 'Kolom logo harus di isi.',
            'logo.dimensions'=> 'Ukuran logo minimal 100 x 100 px.',
            'logo.mimes' => 'format logo harus .png.',
            'logo.max' => 'ukuran logo maksimal 2mb.'
        ];
    }
}
