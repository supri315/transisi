<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'nama',
        'company_id',
        'email'
    ];

    public function company(){
    	return $this->belongsTo('App\Models\Company');
    }

}
