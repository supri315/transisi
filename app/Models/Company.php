<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
	use SoftDeletes;

   	protected $fillable = [
    	'nama',
        'email',
        'logo',
        'website'
    ];

    public function companies(){ 
    	return $this->hasMany('App\Models\Employee'); 
	}
}
